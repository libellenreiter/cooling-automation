#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <time.h>
#include <TimeLib.h>
#include <WiFiUdp.h>

const int timeZone = 1;     // Central European Time

// Libraries for the DS18B20 Temperature Sensor
#include <OneWire.h>
#include <DallasTemperature.h>

#define WIFI_SSID "YOUR_SSID"
#define WIFI_PASS "YOUR_WIFI_PASSWORD"
#define SERIAL_BAUDRATE 115200
#define TIMER 600000

// Output Relay
#define RelayOnPin 7
#define RelayOffPin 8

// One-Wire Temperature Sensor
// (Use GPIO pins for power/ground to simplify the wiring)
#define ONE_WIRE_BUS 2
#define ONE_WIRE_PWR 3
#define ONE_WIRE_GND 4

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);
 
// Pass our oneWire reference to Dallas Temperature. 
DallasTemperature sensors(&oneWire);
 
// arrays to hold device address
DeviceAddress tempSensor;

// URI to get the time
const char* time_uri "http://worldtimeapi.org/api/timezone/Europe/Berlin.txt";


void wifiSetup() {

	// Set WIFI module to STA mode
	WiFi.mode(WIFI_STA);

	// Connect
	Serial.printf("[WIFI] Connecting to %s ", WIFI_SSID);
	WiFi.begin(WIFI_SSID, WIFI_PASS);

	// Wait
	while (WiFi.status() != WL_CONNECTED) {
		Serial.print(".");
		delay(100);
	}
	Serial.println();

	// Connected!
	Serial.printf("[WIFI] STATION Mode, SSID: %s, IP address: %s\n", WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());
}

void tooglerelay(char state) {
	if(state == "on") {
		digitalWrite(RelayOnPin, HIGH);
		delay(10);
		digitalWrite(RelayOnPin, LOW);
	}
	else {
		digitalWrite(RelayOffPin, HIGH);
		delay(10);
		digitalWrite(RelayOffPin, LOW);
	}
}

int measuretemperatur() {
	Serial.print("Requesting temperatures...");
	sensors.requestTemperatures(); // Send the command to get temperatures
	Serial.println("DONE");
	// After we got the temperatures, we can print them here.
	// We use the function ByIndex, and as an example get the temperature from the first sensor only.
	float temperatur = sensors.getTempCByIndex(0);

	// Check if reading was successful
	if(temperatur != DEVICE_DISCONNECTED_C) {
		Serial.print("Temperature for the device 1 (index 0) is: ");
		Serial.println(temperatur);
		return temperatur;
	} 
	else {
	Serial.println("Error: Could not read temperature data");
	}
}

int time() {
	time_t now = time(nullptr);
	Serial.println(ctime(&now));
	return hour(now);
}

// Bug workaround for Arduino 1.6.6, it seems to need a function declaration
// for some reason (only affects ESP8266, likely an arduino-builder bug).
//void MQTT_connect();

void setup() {
	// Init serial port and clean garbage
	Serial.begin(SERIAL_BAUDRATE);
	Serial.println("");

	// Initialize Relay Control:

	pinMode(RelayOnPin, OUTPUT);    // Output mode to drive on relay
	pinMode(RelayOffPin, OUTPUT);    // Output mode to drive off relay
	tooglerelay("off")  // make sure it is off to start
 
	// Set up Ground & Power for the sensor from GPIO pins

	pinMode(ONE_WIRE_GND, OUTPUT);
	digitalWrite(ONE_WIRE_GND, LOW);

	pinMode(ONE_WIRE_PWR, OUTPUT);
	digitalWrite(ONE_WIRE_PWR, HIGH);

	// Start up the DS18B20 One Wire Temperature Sensor

	sensors.begin();
	//sensors.setResolution(tempSensor, 12);
	//sensors.setWaitForConversion(false);

	// Wifi
	wifiSetup();

	if(state) {
		Serial.println("ON!");
	}
	else {
		Serial.println("off");
	}

	// Setup NTP Time
	configTime(3 * 3600, 0, "pool.ntp.org", "time.nist.gov");
	Serial.println("\nWaiting for time");
	while (!time(nullptr)) {
		Serial.print(".");
		delay(1000);
	}

	// ************************************************
	// Timer Interrupt Handler
	// ************************************************
	SIGNAL(TIMER2_OVF_vect) {
	if (opState == OFF) {
		digitalWrite(RelayPin, LOW);  // make sure relay is off
	}
	else {
		DriveOutput();
	}
	
	// Setup MQTT will to set on/off to "OFF" when we disconnect
	//mqtt.will(WILL_FEED, "OFF"); 
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
//void MQTT_connect() {
//	int8_t ret;
//
//	// Stop if already connected.
//	if (mqtt.connected()) {
//		return;
//	}
//
//	Serial.print("Connecting to MQTT... ");
//
//	uint8_t retries = 3;
//	while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
//		Serial.println(mqtt.connectErrorString(ret));
//		Serial.println("Retrying MQTT connection in 5 seconds...");
//		mqtt.disconnect();
//		delay(5000);  // wait 5 seconds
//		retries--;
//		if (retries == 0) {
//			// basically die and wait for WDT to reset me
//			while (1);
//		}
//	}
//	Serial.println("MQTT Connected!");
//}

void loop() {
	clock++;
	
	//send teamperatur to server
	//MQTT_connect();
	//lastwill.publish("ON");  // make sure we publish ON first thing after connecting
	// Now we can publish stuff!
	//Serial.print(F("\nSending photocell val "));
	//Serial.print(x);
	//Serial.print("...");
	//if (! photocell.publish(x++)) {
	//	Serial.println(F("Failed"));
	//} 
	//else {
	//	Serial.println(F("OK!"));
	//}

	//automation of relay depending on temperatur
	if (time() >= 22 && time() < 7) {
		if (clock >= timer) {
			if (measuretemperatur() >= 22) {
				tooglerelay("on");
			}
			else {
				tooglerelay("off");
			}
			clock = 0;
		}
	}
	else {
		tooglerelay("off");
	}
}