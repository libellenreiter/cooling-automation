import time
import board
import digitalio
from adafruit_onewire.bus import OneWireBus
from adafruit_ds18x20 import DS18X20
from adafruit_ntp import NTP
import adafruit_miniesptool

# Initialize pins for tooglerelay
relayon = digitalio.DigitalInOut(board.DX)
relayon.direction = digitalio.Direction.OUTPUT
relayoff = digitalio.DigitalInOut(board.DX)
relayoff.direction = digitalio.Direction.OUTPUT

# Initialize one-wire bus on board pin D5.
ow_bus = OneWireBus(board.D5)

# Scan for sensors and grab the first one found.
ds18 = DS18X20(ow_bus, ow_bus.scan()[0])

def temperatur ():
	print('Temperature: {0:0.3f}C'.format(ds18.temperature))

# Main loop to print the temperature every second.
while True:
	temperature()
	time.sleep(60.0)